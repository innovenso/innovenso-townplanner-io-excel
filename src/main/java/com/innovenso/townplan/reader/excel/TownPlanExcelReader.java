package com.innovenso.townplan.reader.excel;

import com.innovenso.townplan.api.command.*;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.aspects.*;
import com.innovenso.townplan.api.value.enums.*;
import com.innovenso.townplan.api.value.it.ItSystemIntegration;
import com.innovenso.townplan.api.value.it.Technology;
import com.innovenso.townplan.api.value.it.decision.DecisionContextType;
import com.innovenso.townplan.api.value.it.decision.DecisionOptionVerdict;
import com.innovenso.townplan.api.value.it.decision.DecisionStatus;
import com.innovenso.townplan.api.value.view.StepKeyValuePair;
import com.innovenso.townplan.domain.TownPlanImpl;
import com.innovenso.townplan.reader.TownPlanReader;
import com.innovenso.townplan.reader.exceptions.TownPlanReaderException;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Log4j2
public class TownPlanExcelReader implements TownPlanReader {
	private final TownPlanImpl townPlan;
	private final XSSFWorkbook workbook;

	public TownPlanExcelReader(@NonNull final TownPlanImpl townPlan, @NonNull final File excelFile) {
		this.townPlan = townPlan;

		try (InputStream inputStream = new FileInputStream(excelFile)) {
			ZipSecureFile.setMinInflateRatio(0.001);
			this.workbook = new XSSFWorkbook(inputStream);
		} catch (IOException e) {
			throw new TownPlanReaderException(e);
		}
	}

	public TownPlanImpl read() {
		load();
		enrich();
		return this.townPlan;
	}

	private void load() {
		log.warn("loading excel file");
		loadKeyPointsInTime();
		loadEnterprises();
		loadPrinciples();
		loadProjects();
		loadProjectMilestones();
		loadBusinessCapabilities();
		loadArchitectureBuildingBlocks();
		loadPlatforms();
		loadSystems();
		loadSystemIntegrations();
		loadTechnologies();
		loadContainers();
		loadEntities();
		loadEntityFields();
		loadActors();
		loadAwsAccounts();
		loadAwsRegions();
		loadAwsVpcs();
		loadAwsAvailabilityZones();
		loadAwsNetworkAcls();
		loadAwsRouteTables();
		loadAwsSubnets();
		loadAwsSecurityGroups();
		loadAwsInstances();
		loadAwsSecurityGroupInstanceMappings();
		loadAwsSubnetInstanceMappings();
		loadAwsAutoScalingGroups();
		loadAwsAutoScalingGroupAvailabilityZones();
		loadAwsAutoScalingGroupSubnets();
		loadAwsAutoScalingGroupInstances();
		loadAwsElasticIpAddress();
		loadDecisions();
		loadDecisionContexts();
		loadDecisionOptions();
		loadRelationships();
		loadSwots();
		loadLifecycles();
		loadArchitectureVerdicts();
		loadDocumentations();
		loadExternalIds();
		loadSecurityConcerns();
		loadSecurityImpacts();
		loadConstraints();
		loadFunctionalRequirements();
		loadQualityAttributeRequirements();
		loadConstraintScores();
		loadFunctionalRequirementScores();
		loadQualityAttributeRequirementScores();
		loadCosts();
		loadFatherTime();
		loadAttachments();
		loadSystemIntegrationSteps();
		loadFlowViews();
		log.warn("excel file upload complete");
	}

	void loadKeyPointsInTime() {
		withSheet("Time Machine",
				row -> this.townPlan.execute(AddKeyPointInTimeCommand.builder()
						.date(LocalDate.of(getIntValue(row, 0), getIntValue(row, 1), getIntValue(row, 2)))
						.title(getStringValue(row, 3)).diagramsNeeded(getBooleanValue(row, 4)).build()));
	}

	void loadEnterprises() {
		withSheet("Enterprises", row -> {
			this.townPlan.execute(AddEnterpriseCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).build());
		});
	}

	void loadPrinciples() {
		withSheet("IT Principles",
				row -> this.townPlan.execute(AddPrincipleCommand.builder().key(getKeyValue(row, 0))
						.title(getStringValue(row, 1)).description(getStringValue(row, 2)).type(getStringValue(row, 3))
						.enterprise(getKeyValue(row, 4)).sortKey(getStringValue(row, 5)).build()));
	}

	void loadDecisions() {
		withSheet("Architecture Decisions",
				row -> this.townPlan.execute(AddDecisionCommand.builder().key(getKeyValue(row, 0))
						.title(getStringValue(row, 1)).description(getStringValue(row, 2))
						.status(DecisionStatus.getDecisionStatus(getStringValue(row, 3)).orElse(DecisionStatus.PENDING))
						.outcome(getStringValue(row, 4)).enterprise(getKeyValue(row, 5)).build()));
	}

	void loadDecisionContexts() {
		withSheet("Decision Contexts", row -> this.townPlan.execute(AddDecisionContextCommand.builder()
				.key(getKeyValue(row, 0)).title(getStringValue(row, 1)).description(getStringValue(row, 2))
				.sortKey(getStringValue(row, 3)).decision(getKeyValue(row, 4)).contextType(DecisionContextType
						.getDecisionContextType(getStringValue(row, 5)).orElse(DecisionContextType.CURRENT_STATE))
				.build()));
	}

	void loadDecisionOptions() {
		withSheet("Decision Options",
				row -> this.townPlan.execute(AddDecisionOptionCommand.builder().key(getKeyValue(row, 0))
						.title(getStringValue(row, 1)).description(getStringValue(row, 2))
						.sortKey(getStringValue(row, 3)).decision(getKeyValue(row, 4))
						.verdict(DecisionOptionVerdict.getDecisionOptionVerdict(getStringValue(row, 5))
								.orElse(DecisionOptionVerdict.UNDER_INVESTIGATION))
						.build()));
	}

	void loadProjects() {
		withSheet("IT Projects", row -> {
			this.townPlan.execute(AddItProjectCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).type(getStringValue(row, 3)).enterprise(getKeyValue(row, 4))
					.build());
		});
	}

	void loadProjectMilestones() {
		withSheet("IT Project Milestones", row -> {
			this.townPlan.execute(AddItProjectMilestoneCommand.builder().key(getKeyValue(row, 0))
					.title(getStringValue(row, 1)).description(getStringValue(row, 2)).project(getKeyValue(row, 3))
					.sortKey(getStringValue(row, 4)).build());
		});
	}

	void loadBusinessCapabilities() {
		Map<String, BusinessCapabilityReadModel> readModel = new HashMap<>();

		withSheet("Business Capabilities - Domains", row -> {
			String key = getKeyValue(row, 0);
			readModel.put(key,
					BusinessCapabilityReadModel.builder().key(key).title(getStringValue(row, 1))
							.description(getStringValue(row, 2)).enterprise(getStringValue(row, 3))
							.parent(getKeyValue(row, 4)).sortKey(getStringValue(row, 5)).build());
		});
		// build tree
		buildCapabilityTree(readModel).forEach(this::loadBusinessCapability);
	}

	private Set<BusinessCapabilityReadModel> buildCapabilityTree(
			final Map<String, BusinessCapabilityReadModel> readModel) {
		readModel.values().stream().filter(row -> readModel.containsKey(row.getParent()))
				.forEach(row -> readModel.get(row.getParent()).getChildren().add(row));
		return readModel.values().stream().filter(row -> StringUtils.isEmpty(row.getParent()))
				.collect(Collectors.toSet());
	}

	private void loadBusinessCapability(final BusinessCapabilityReadModel readModel) {
		townPlan.execute(AddCapabilityCommand.builder().key(readModel.getKey()).title(readModel.getTitle())
				.description(readModel.getDescription()).enterprise(readModel.getEnterprise())
				.parent(readModel.getParent()).sortKey(readModel.getSortKey()).build());
		if (!readModel.getChildren().isEmpty()) {
			readModel.getChildren().forEach(this::loadBusinessCapability);
		}
	}

	void loadArchitectureBuildingBlocks() {
		withSheet("Architecture Building Blocks", row -> {
			townPlan.execute(AddBuildingBlockCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).enterprise(getKeyValue(row, 3)).build());
		});
	}

	private void withSheet(@NonNull final String name, @NonNull final Consumer<Row> rowConsumer) {
		final Sheet sheet = workbook.getSheet(name);
		if (sheet == null) {
			log.error("Sheet not found: {}", name);
			return;
		}
		log.info("Loading Excel sheet {}", name);
		sheet.forEach(row -> {
			if (row.getRowNum() > 0) {
				log.info("loading row {} from sheet {}", row.getRowNum(), name);
				rowConsumer.accept(row);
			}
		});
	}

	void loadPlatforms() {
		withSheet("IT Platforms", row -> {
			townPlan.execute(AddITPlatformCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).build());
		});
	}

	void loadSystems() {
		withSheet("IT Systems", row -> {
			townPlan.execute(AddITSystemCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).platform(getKeyValue(row, 3)).build());
		});
	}

	void loadSystemIntegrations() {
		withSheet("IT System Integrations", row -> {
			townPlan.execute(AddITSystemIntegrationCommand.builder().key(getKeyValue(row, 0))
					.title(getStringValue(row, 1)).description(getStringValue(row, 2)).sourceSystem(getKeyValue(row, 3))
					.targetSystem(getKeyValue(row, 4)).volume(getStringValue(row, 5)).frequency(getStringValue(row, 6))
					.criticality(Criticality.getCriticality(getStringValue(row, 7)).orElse(Criticality.UNKNOWN))
					.criticalityDescription(getStringValue(row, 8)).resilience(getStringValue(row, 9)).build());
		});
	}

	void loadTechnologies() {
		withSheet("IT Technologies", row -> {
			townPlan.execute(AddTechnologyCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).type(getStringValue(row, 3))
					.recommendation(TechnologyRecommendation.getTechnologyRecommendation(getStringValue(row, 4))
							.orElse(TechnologyRecommendation.UNDEFINED))
					.technologyType(
							TechnologyType.getTechnologyType(getStringValue(row, 5)).orElse(TechnologyType.UNDEFINED))
					.build());
		});
	}

	void loadContainers() {
		withSheet("IT Containers", row -> {
			townPlan.execute(AddITContainerCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2))
					.type(ContainerType.getContainerType(getStringValue(row, 3)).orElse(ContainerType.OTHER))
					.system(getKeyValue(row, 4)).build());
		});
	}

	void loadEntities() {
		withSheet("Data Entities", row -> {
			String key = getKeyValue(row, 0);
			townPlan.execute(AddDataEntityCommand.builder().key(key).title(getStringValue(row, 1))
					.description(getStringValue(row, 2))
					.entityType(EntityType.getEntityType(getStringValue(row, 3)).orElse(EntityType.UNDEFINED))
					.sensitivity(getStringValue(row, 4)).commercialValue(getStringValue(row, 5))
					.consentNeeded(getBooleanValue(row, 6)).masterSystem(getKeyValue(row, 7))
					.businessCapability(getKeyValue(row, 8)).enterprise(getKeyValue(row, 9)).build());
		});
	}

	void loadEntityFields() {
		withSheet("Data Entity Fields", row -> {
			String entityKey = getKeyValue(row, 0);
			String fieldKey = getKeyValue(row, 1);
			townPlan.execute(AddDataEntityFieldCommand.builder().key(fieldKey).dataEntityKey(entityKey)
					.title(getStringValue(row, 2)).description(getStringValue(row, 3))
					.mandatory(getBooleanValue(row, 4)).unique(getBooleanValue(row, 4))
					.fieldConstraints(getStringValue(row, 5)).defaultValue(getStringValue(row, 6))
					.localization(getStringValue(row, 7)).sortKey(getStringValue(row, 8)).type(getStringValue(row, 9))
					.build());
		});
	}

	void loadActors() {
		withSheet("Business Actors", row -> {
			townPlan.execute(AddActorCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2))
					.type(ActorType.getActorType(getStringValue(row, 3)).orElse(ActorType.OTHER))
					.enterprise(getKeyValue(row, 4)).build());
		});
	}

	void loadAwsRegions() {
		withSheet("AWS Regions", row -> {
			townPlan.execute(AddAwsRegionCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).build());
		});
	}

	void loadAwsVpcs() {
		withSheet("AWS VPCs", row -> {
			townPlan.execute(AddAwsVpcCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).region(getKeyValue(row, 3)).networkMask(getStringValue(row, 4))
					.account(getKeyValue(row, 5)).build());
		});
	}

	void loadAwsAvailabilityZones() {
		withSheet("AWS AZs", row -> {
			townPlan.execute(
					AddAwsAvailabilityZoneCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
							.description(getStringValue(row, 2)).region(getKeyValue(row, 3)).build());
		});
	}

	void loadAwsNetworkAcls() {
		withSheet("AWS Network ACL", row -> {
			townPlan.execute(AddAwsNetworkAclCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).vpc(getKeyValue(row, 3)).build());
		});
	}

	void loadAwsRouteTables() {
		withSheet("AWS Route Tables", row -> {
			townPlan.execute(AddAwsRouteTableCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).vpc(getKeyValue(row, 3)).build());
		});
	}

	void loadAwsSubnets() {
		withSheet("AWS Subnets", row -> {
			townPlan.execute(AddAwsSubnetCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).vpc(getKeyValue(row, 3)).availabilityZone(getKeyValue(row, 4))
					.subnetMask(getStringValue(row, 5)).publicSubnet(getBooleanValue(row, 6)).build());
		});
	}

	void loadAwsSecurityGroups() {
		withSheet("AWS Security Groups", row -> {
			townPlan.execute(AddAwsSecurityGroupCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).vpc(getKeyValue(row, 3)).build());
		});
	}

	void loadAwsInstances() {
		withSheet("AWS Instances", row -> {
			townPlan.execute(AddAwsInstanceCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2))
					.instanceType(AwsInstanceType.getAwsInstanceType(getStringValue(row, 3))).vpc(getKeyValue(row, 4))
					.account(getKeyValue(row, 5)).region(getKeyValue(row, 6)).build());
		});
	}

	void loadAwsSecurityGroupInstanceMappings() {
		withSheet("AWS Security Group Map", row -> {
			townPlan.execute(AssignSecurityGroupToInstanceCommand.builder().securityGroupKey(getKeyValue(row, 0))
					.instanceKey(getKeyValue(row, 1)).build());
		});
	}

	void loadAwsSubnetInstanceMappings() {
		withSheet("AWS Subnet Map", row -> {
			townPlan.execute(AssignSubnetToInstanceCommand.builder().subnetKey(getKeyValue(row, 0))
					.instanceKey(getKeyValue(row, 1)).build());
		});
	}

	void loadAwsAccounts() {
		withSheet("Aws Accounts", row -> {
			townPlan.execute(AddAwsAccountCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).accountId(getStringValue(row, 3)).build());
		});
	}

	void loadAwsAutoScalingGroups() {
		withSheet("AWS Autoscaling Groups", row -> {
			townPlan.execute(
					AddAwsAutoScalingGroupCommand.builder().key(getKeyValue(row, 0)).title(getStringValue(row, 1))
							.description(getStringValue(row, 2)).vpc(getKeyValue(row, 3)).build());
		});
	}

	void loadAwsAutoScalingGroupAvailabilityZones() {
		withSheet("AWS Autoscaling AZs", row -> {
			townPlan.execute(AssignAvailabilityZoneToAutoScalingGroupCommand.builder()
					.autoScalingGroup(getKeyValue(row, 0)).availabilityZone(getKeyValue(row, 1)).build());
		});
	}

	void loadAwsAutoScalingGroupSubnets() {
		withSheet("AWS Autoscaling Subnets", row -> {
			townPlan.execute(AssignSubnetToAutoScalingGroupCommand.builder().autoscalingGroup(getKeyValue(row, 0))
					.subnet(getKeyValue(row, 1)).build());
		});
	}

	void loadAwsAutoScalingGroupInstances() {
		withSheet("AWS Autoscaling Instances", row -> {
			townPlan.execute(AssignInstanceToAutoScalingGroupCommand.builder().autoscalingGroup(getKeyValue(row, 0))
					.instance(getKeyValue(row, 1)).build());
		});
	}

	void loadAwsElasticIpAddress() {
		withSheet("AWS IP Addresses", row -> {
			townPlan.execute(AddAwsElasticIpAddressCommand.builder().key(getKeyValue(row, 0))
					.title(getStringValue(row, 1)).description(getStringValue(row, 2)).region(getKeyValue(row, 3))
					.publicIpv4Pool(getStringValue(row, 4)).publicIpAddress(getStringValue(row, 5))
					.privateIpAddress(getStringValue(row, 6)).networkInterfaceOwnerId(getStringValue(row, 7))
					.networkInterfaceId(getStringValue(row, 8)).networkBorderGroup(getStringValue(row, 9))
					.instanceId(getStringValue(row, 10)).domain(getStringValue(row, 11))
					.customerOwnedIpv4Pool(getStringValue(row, 12)).customerOwnedIp(getStringValue(row, 13))
					.carrierIp(getStringValue(row, 14)).associationId(getStringValue(row, 15))
					.allocationId(getStringValue(row, 16)).account(getKeyValue(row, 17)).instance(getKeyValue(row, 18))
					.build());
		});
	}

	void loadSwots() {
		withSheet("Aspects - SWOT", row -> {
			Optional<SWOTType> typeOptional = SWOTType.getSwotType(getStringValue(row, 1));
			if (typeOptional.isPresent()) {
				townPlan.execute(AddSwotAspectCommand.builder().conceptKey(getKeyValue(row, 0)).type(typeOptional.get())
						.description(getStringValue(row, 2)).build());
			} else {
				log.error("Could not find SWOT Type {}", getStringValue(row, 1));
			}
		});
	}

	void loadDocumentations() {
		withSheet("Aspects - Documentation", row -> {
			Optional<DocumentationType> typeOptional = DocumentationType.getDocumentationType(getStringValue(row, 1));
			if (typeOptional.isPresent()) {
				townPlan.execute(
						AddDocumentationAspectCommand.builder().conceptKey(getKeyValue(row, 0)).type(typeOptional.get())
								.description(getStringValue(row, 2)).sortKey(getStringValue(row, 3)).build());
			} else {
				log.error("Could not find Documentation Type {}", getStringValue(row, 1));
			}
		});
	}

	void loadExternalIds() {
		withSheet("Aspects - External ID",
				row -> ExternalIdType.getExternalIdType(getStringValue(row, 1)).ifPresent(
						externalIdType -> townPlan.execute(AddExternalIdAspectCommand.builder().type(externalIdType)
								.conceptKey(getKeyValue(row, 0)).value(getStringValue(row, 2)).build())));
	}

	void loadSecurityConcerns() {
		withSheet("Aspects - Security Concern", row -> {
			SecurityConcernType.getSecurityConcernType(getStringValue(row, 1))
					.ifPresent(securityConcernType -> townPlan
							.execute(AddSecurityConcernAspectCommand.builder().modelComponentKey(getKeyValue(row, 0))
									.concernType(securityConcernType).title(getStringValue(row, 2))
									.description(getStringValue(row, 3)).sortKey(getStringValue(row, 4)).build()));
		});
	}

	void loadSecurityImpacts() {
		withSheet("Aspects - Security Impact", row -> {
			SecurityImpactLevel.getSecurityImpactLevel(getStringValue(row, 1))
					.ifPresent(securityImpactLevel -> SecurityImpactType.getSecurityImpactType(getStringValue(row, 2))
							.ifPresent(securityImpactType -> townPlan.execute(AddSecurityImpactAspectCommand.builder()
									.modelComponentKey(getKeyValue(row, 0)).impactLevel(securityImpactLevel)
									.impactType(securityImpactType).description(getStringValue(row, 3)).build())));
		});
	}

	void loadConstraints() {
		withSheet("Aspects - Constraints", row -> {
			townPlan.execute(AddConstraintAspectCommand
					.builder().modelComponentKey(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).sortKey(getStringValue(row, 3)).weight(RequirementWeight
							.getRequirementWeight(getStringValue(row, 4)).orElse(RequirementWeight.SHOULD_HAVE))
					.key(getOptionalKeyValue(row, 5)).build());
		});
	}

	void loadConstraintScores() {
		withSheet("Aspects - Constraint Scores", row -> {
			townPlan.execute(AddConstraintScoreAspectCommand.builder().modelComponentKey(getKeyValue(row, 0))
					.constraintKey(getKeyValue(row, 1)).description(getStringValue(row, 2))
					.weight(ScoreWeight.getScoreWeight(getStringValue(row, 3)).orElse(ScoreWeight.UNKNOWN))
					.key(getOptionalKeyValue(row, 4)).build());
		});
	}

	void loadFunctionalRequirements() {
		withSheet("Aspects - Functional Reqs", row -> {
			townPlan.execute(AddFunctionalRequirementAspectCommand
					.builder().modelComponentKey(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.description(getStringValue(row, 2)).sortKey(getStringValue(row, 3)).weight(RequirementWeight
							.getRequirementWeight(getStringValue(row, 4)).orElse(RequirementWeight.SHOULD_HAVE))
					.key(getOptionalKeyValue(row, 5)).build());
		});
	}

	void loadFunctionalRequirementScores() {
		withSheet("Aspects - Functional Scores", row -> {
			townPlan.execute(AddFunctionalRequirementScoreAspectCommand.builder().modelComponentKey(getKeyValue(row, 0))
					.functionalRequirementKey(getKeyValue(row, 1)).description(getStringValue(row, 2))
					.weight(ScoreWeight.getScoreWeight(getStringValue(row, 3)).orElse(ScoreWeight.UNKNOWN))
					.key(getOptionalKeyValue(row, 4)).build());
		});
	}

	void loadQualityAttributeRequirementScores() {
		withSheet("Aspects - QAR Scores", row -> {
			townPlan.execute(
					AddQualityAttributeRequirementScoreAspectCommand.builder().modelComponentKey(getKeyValue(row, 0))
							.qualityAttributeRequirementKey(getKeyValue(row, 1)).description(getStringValue(row, 2))
							.weight(ScoreWeight.getScoreWeight(getStringValue(row, 3)).orElse(ScoreWeight.UNKNOWN))
							.key(getOptionalKeyValue(row, 4)).build());
		});
	}

	void loadQualityAttributeRequirements() {
		withSheet("Aspects - QAR", row -> {
			townPlan.execute(AddQualityAttributeRequirementAspectCommand.builder()
					.modelComponentKey(getKeyValue(row, 0)).title(getStringValue(row, 1))
					.sourceOfStimulus(getStringValue(row, 2)).stimulus(getStringValue(row, 3))
					.environment(getStringValue(row, 4)).response(getStringValue(row, 5))
					.responseMeasure(getStringValue(row, 6)).sortKey(getStringValue(row, 7)).weight(RequirementWeight
							.getRequirementWeight(getStringValue(row, 8)).orElse(RequirementWeight.SHOULD_HAVE))
					.key(getOptionalKeyValue(row, 9)).build());
		});
	}

	void loadCosts() {
		withSheet("Aspects - Cost",
				row -> CostType.getCostType(getStringValue(row, 8))
						.ifPresent(costType -> townPlan.execute(AddCostAspectCommand.builder()
								.key(UUID.randomUUID().toString().replaceAll("-", "_")).conceptKey(getKeyValue(row, 0))
								.title(getStringValue(row, 1)).description(getStringValue(row, 2))
								.category(getStringValue(row, 3)).fiscalYear(getIntValue(row, 4))
								.costPerUnit(getDoubleValue(row, 5)).unitOfMeasure(getStringValue(row, 6))
								.numberOfUnits(getDoubleValue(row, 7)).costType(costType).build())));
	}

	void loadFatherTime() {
		withSheet("Aspects - Father Time", row -> FatherTimeType.getFatherTimeType(getStringValue(row, 6))
				.ifPresent(fatherTimeType -> townPlan.execute(AddFatherTimeAspectCommand.builder()
						.key(UUID.randomUUID().toString().replaceAll("-", "_")).conceptKey(getKeyValue(row, 0))
						.title(getStringValue(row, 1)).description(getStringValue(row, 2))
						.pointInTime(LocalDate.of(getIntValue(row, 3), getIntValue(row, 4), getIntValue(row, 5)))
						.fatherTimeType(fatherTimeType).build())));
	}

	void loadAttachments() {
		withSheet("Aspects - Attachments",
				row -> townPlan.execute(
						AddAttachmentAspectCommand.builder().key(UUID.randomUUID().toString().replaceAll("-", "_"))
								.conceptKey(getKeyValue(row, 0)).title(getStringValue(row, 1))
								.description(getStringValue(row, 2)).cdnContentUrl(getStringValue(row, 3)).build()));
	}

	void loadLifecycles() {
		withSheet("Aspects - lifecycle", row -> {
			Optional<LifecyleType> typeOptional = LifecyleType.getLifecycleType(getStringValue(row, 1));
			if (typeOptional.isPresent()) {
				townPlan.execute(SetLifecycleAspectCommand.builder().conceptKey(getKeyValue(row, 0))
						.lifecyleType(typeOptional.get()).description(getStringValue(row, 2)).build());
			} else {
				log.error("Could not find Lifecycle type {}", getStringValue(row, 1));
			}
		});
	}

	void loadArchitectureVerdicts() {
		withSheet("Aspects - architecture verdict",
				row -> ArchitectureVerdictType.getArchitectureVerdictType(getStringValue(row, 1))
						.ifPresent(architectureVerdictType -> townPlan.execute(SetArchitectureVerdictAspectCommand
								.builder().conceptKey(getKeyValue(row, 0)).verdictType(architectureVerdictType)
								.description(getStringValue(row, 2)).build())));
	}

	private void loadRelationships() {
		withSheet("Relationships", row -> {
			townPlan.execute(AddRelationshipCommand.builder().source(getKeyValue(row, 0)).target(getKeyValue(row, 1))
					.title(getStringValue(row, 2)).description(getStringValue(row, 3))
					.type(RelationshipType.getRelationshipType(getStringValue(row, 5)).orElse(RelationshipType.FLOW))
					.bidirectional(getBooleanValue(row, 4)).influence(getIntValue(row, 6)).interest(getIntValue(row, 7))
					.integrationIdentifier(getStringValue(row, 8)).build());
		});
	}

	public void enrich() {
		enrichContainerTechnologies();
		enrichRelationshipTechnologies();
		// enrichViews();
	}

	private void enrichContainerTechnologies() {
		withSheet("IT Container Technology Mapping", row -> {
			if (!Objects.equals(getKeyValue(row, 1), Technology.UNKNOWN)) {
				townPlan.execute(AddITContainerTechnologyCommand.builder().container(getKeyValue(row, 0))
						.technology(getKeyValue(row, 1)).build());
			}
		});
	}

	private void enrichRelationshipTechnologies() {
		withSheet("Relationship Technology Mapping", row -> {
			if (!Objects.equals(getKeyValue(row, 1), Technology.UNKNOWN)) {
				townPlan.execute(AddRelationshipTechnologyCommand.builder().relationship(getKeyValue(row, 0))
						.technology(getKeyValue(row, 1)).build());
			}
		});
	}

	private void loadFlowViews() {
		withSheet("Flow Views", row -> {
			int number = getIntValue(row, 0);
			String key = getKeyValue(row, 1);
			townPlan.execute(AddFlowViewCommand.builder().key(key).title(getStringValue(row, 2))
					.description(getStringValue(row, 3)).showStepCounter(getBooleanValue(row, 4)).build());
			loadFlowView(key, number);
		});
	}

	private void loadSystemIntegrationSteps() {
		townPlan.getElements(ItSystemIntegration.class)
				.forEach(integration -> loadSystemIntegration(integration.getKey()));
	}

	private void loadSystemIntegration(String key) {
		List<StepKeyValuePair> steps = new ArrayList<>();
		withSheet(DigestUtils.sha1Hex(key.getBytes()).substring(0, 31), row -> {
			String relationshipSource = getKeyValue(row, 0);
			String relationshipTarget = getKeyValue(row, 1);
			String relationshipType = getStringValue(row, 2);
			String titleOverride = getStringValue(row, 3);
			boolean response = getBooleanValue(row, 4);
			townPlan.getRelationship(relationshipSource, relationshipTarget,
					RelationshipType.getRelationshipType(relationshipType).orElse(RelationshipType.FLOW))
					.ifPresent(relationship -> steps.add(StepKeyValuePair.builder().response(response)
							.title(titleOverride).relationship(relationship.getKey()).build()));
		});
		townPlan.execute(SetSystemIntegrationStepsCommand.builder().key(key).steps(steps).build());
	}

	private void loadFlowView(String key, int number) {
		withSheet("Flow View - " + number, row -> {
			String relationshipSource = getKeyValue(row, 0);
			String relationshipTarget = getKeyValue(row, 1);
			String relationshipType = getStringValue(row, 2);
			String titleOverride = getStringValue(row, 3);
			boolean response = getBooleanValue(row, 4);

			log.info("import step {}_{}_{}: {}", relationshipSource, relationshipType, relationshipTarget,
					titleOverride);
			RelationshipType relType = RelationshipType.getRelationshipType(relationshipType)
					.orElse(RelationshipType.FLOW);
			townPlan.getRelationship(relationshipSource, relationshipTarget, relType).ifPresentOrElse(
					relationship -> townPlan
							.execute(AddFlowViewStepCommand.builder().relationship(relationship.getKey())
									.title(getStringValue(row, 3)).response(response).view(key).build()),
					() -> log.warn("relationship {}_{}_{} not found", relationshipSource, relationshipType,
							relationshipTarget));
		});
	}

	//
	// private void loadViews() {
	// Sheet viewSheet = workbook.getSheet("Views");
	// viewSheet.forEach(row -> {
	// if (row.getRowNum() > 0) {
	// AbstractView view = AbstractView.builder()
	// .label(getStringValue(row, 0))
	// .title(getStringValue(row, 1))
	// .description(getStringValue(row, 2))
	// .build();
	//
	// views.put(view.getLabel(), view);
	// }
	// });
	// }
	//
	// private void enrichViews() {
	// Sheet viewMappingSheet = workbook.getSheet("AbstractView Mappings");
	// viewMappingSheet.forEach(row -> {
	// String viewName = getStringValue(row, 0);
	// AbstractView view = views.get(viewName);
	// if (view != null) {
	// BusinessCapability businessCapability =
	// businessCapabilities.get(getStringValue(row, 1));
	// ArchitectureBuildingBlock buildingBlock =
	// itBuildingBlocks.get(getStringValue(row, 2));
	// ItSystem itSystem = systems.get(getStringValue(row, 3));
	// ItSystem systemContext = systems.get(getStringValue(row, 4));
	// Container container = containers.get(getStringValue(row, 5));
	// Actor actor = actors.get(getStringValue(row, 6));
	// DataEntity entity = entities.get(getStringValue(row, 7));
	// if (itSystem != null) {
	// view.getSystems().add(itSystem);
	// }
	// if (businessCapability != null) {
	// view.getBusinessCapabilities().add(businessCapability);
	// }
	// if (buildingBlock != null) {
	// view.getBuildingBlocks().add(buildingBlock);
	// }
	// if (systemContext != null) {
	// view.getSystemContexts().add(systemContext);
	// }
	// if (container != null) {
	// view.getContainers().add(container);
	// }
	// if (actor != null) {
	// view.getActors().add(actor);
	// }
	// if (entity != null) {
	// view.getEntities().add(entity);
	// }
	//
	// getActorConnections().forEach(view::maybeAddActorConnection);
	// getItConnections().forEach(view::maybeAddItConnection);
	//
	// getCapabilityBuildingBlockMappings().forEach(view::maybeAddCapabilityBuildingBlockMapping);
	//
	// getBuildingBlockSystemMappings().forEach(view::maybeAddBuildingBlockSystemMapping);
	// }
	// });
	// }

	private String getStringValue(Row row, int columnIndex) {
		Cell cell = row.getCell(columnIndex);
		if (cell == null)
			return "";
		return cell.getStringCellValue();
	}

	private String getKeyValue(Row row, int columnIndex) {
		return getStringValue(row, columnIndex).replaceAll(" ", "_").replaceAll("-", "_");
	}

	private String getOptionalKeyValue(Row row, int columnIndex) {
		final String keyValue = getKeyValue(row, columnIndex);
		return keyValue.isEmpty() ? UUID.randomUUID().toString().replaceAll("-", "_") : keyValue;
	}

	private boolean getBooleanValue(Row row, int columnIndex) {
		Cell cell = row.getCell(columnIndex);
		if (cell == null)
			return false;
		return Boolean.parseBoolean(cell.getStringCellValue());
	}

	private int getIntValue(Row row, int columnIndex) {
		Cell cell = row.getCell(columnIndex);
		if (cell == null)
			return 0;
		return (int) cell.getNumericCellValue();
	}

	private double getDoubleValue(Row row, int columnIndex) {
		Cell cell = row.getCell(columnIndex);
		if (cell == null)
			return 0d;
		return cell.getNumericCellValue();
	}
}
