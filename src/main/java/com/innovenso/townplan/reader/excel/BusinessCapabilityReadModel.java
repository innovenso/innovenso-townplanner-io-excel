package com.innovenso.townplan.reader.excel;

import lombok.Builder;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
@Builder
public class BusinessCapabilityReadModel {
	private String key;
	private String title;
	private String description;
	private String enterprise;
	private String parent;
	private String sortKey;
	@Builder.Default
	private Set<BusinessCapabilityReadModel> children = new HashSet<>();
}
