package com.innovenso.townplan.writer.excel;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.pipeline.AbstractTownPlanWriter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.util.List;

@Log4j2
public class TownPlanExcelWriter extends AbstractTownPlanWriter {
	private File targetBaseDirectory;
	private TownPlanFullExcelWriter fullExcelWriter;
	private TownPlanSystemIntegrationListExcelWriter systemIntegrationListExcelWriter;

	public TownPlanExcelWriter(@NonNull AssetRepository assetRepository, @NonNull final String targetBasePath) {
		super(assetRepository, ContentOutputType.EXCEL);
		setTargetBaseDirectory(new File(targetBasePath, "excel"));
	}

	@Override
	public void setTargetBaseDirectory(@NonNull final File targetBaseDirectory) {
		this.targetBaseDirectory = targetBaseDirectory;
		this.targetBaseDirectory.mkdirs();
		this.fullExcelWriter = new TownPlanFullExcelWriter(targetBaseDirectory);
		this.systemIntegrationListExcelWriter = new TownPlanSystemIntegrationListExcelWriter(targetBaseDirectory);
	}

	@Override
	public File getTargetBaseDirectory() {
		return targetBaseDirectory;
	}

	@Override
	protected void writeFiles(@NonNull TownPlan townPlan, @NonNull List<File> renderedFiles) {
		fullExcelWriter.write(townPlan, renderedFiles);
		systemIntegrationListExcelWriter.write(townPlan, renderedFiles);
	}

	@Override
	protected void writeFiles(@NonNull TownPlan townPlan, @NonNull List<File> renderedFiles, @NonNull String s) {
		log.warn("Writing a single concept is not supported in the Excel writer, writing full town plan instead");
		writeFiles(townPlan, renderedFiles);
	}
}
