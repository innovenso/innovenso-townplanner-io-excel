package com.innovenso.townplan.writer.excel;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.Element;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Function;

@Log4j2
public abstract class AbstractTownPlanExcelWriter {
	private final File targetBaseDirectory;

	public AbstractTownPlanExcelWriter(@NonNull final File targetBaseDirectory) {
		this.targetBaseDirectory = targetBaseDirectory;
		if (!targetBaseDirectory.exists())
			targetBaseDirectory.mkdirs();
	}

	public Optional<File> write(final @NonNull TownPlan townPlan, List<File> renderedFiles) {
		try {
			final File excelOutputFile = File.createTempFile(townPlan.getId().getEntityId().value,
					UUID.randomUUID().toString());

			return writeWorkBook(townPlan, excelOutputFile).map(workbook -> {
				final File resultingFile = new File(targetBaseDirectory, getExcelOutputFileName(townPlan) + ".xlsx");
				try {
					FileUtils.copyFile(excelOutputFile, resultingFile);
					log.info("Excel output file saved {}", resultingFile.getAbsolutePath());
					renderedFiles.add(resultingFile);
				} catch (IOException e) {
					return null;
				}
				return resultingFile;
			});
		} catch (IOException e) {
			log.error(e);
			return Optional.empty();
		}
	}

	private Optional<XSSFWorkbook> writeWorkBook(final @NonNull TownPlan townPlan,
			final @NonNull File excelOutputFile) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		write(townPlan, workbook);
		try (FileOutputStream outputStream = new FileOutputStream(excelOutputFile)) {
			workbook.write(outputStream);
			workbook.close();
			return Optional.of(workbook);
		} catch (IOException e) {
			return Optional.empty();
		}
	}

	protected abstract void write(final @NonNull TownPlan townPlan, @NonNull final XSSFWorkbook workbook);

	protected String getExcelOutputFileName(final @NonNull TownPlan townPlan) {
		return townPlan.getId().getEntityId().value;
	}

	protected Sheet createSheet(String name, Workbook workbook, String... headerTitles) {
		if (workbook.getSheet(name) != null)
			return workbook.getSheet(name);
		final Sheet sheet = workbook.createSheet(name);
		final Row header = sheet.createRow(0);

		for (int i = 0; i < headerTitles.length; i++) {
			createHeaderCell(workbook, sheet, header, i, headerTitles[i]);
		}
		return sheet;
	}

	protected void withSheet(String name, Workbook workbook, List<String> headerTitles,
			BiConsumer<Sheet, AtomicInteger> writeFunction) {
		final Sheet sheet = createSheet(name, workbook, headerTitles.toArray(new String[0]));
		final AtomicInteger counter = new AtomicInteger(1);
		writeFunction.accept(sheet, counter);
	}

	protected <T extends Element> void writeElements(TownPlan townPlan, String sheetName, Workbook workbook,
			List<String> headerTitles, Class<T> elementClass, Function<T, List<Object>> columnSupplier) {
		withSheet(sheetName, workbook, headerTitles,
				(sheet, counter) -> townPlan.getElements(elementClass).stream()
						.sorted(Comparator.comparing(Concept::getKey)).forEach(element -> writeValues(workbook, sheet,
								counter.getAndIncrement(), columnSupplier.apply(element).toArray())));
	}

	protected Sheet getSheet(String name, Workbook workbook) {
		return workbook.getSheet(name);
	}

	protected List<Object> listOf(Object... objects) {
		List<Object> list = new ArrayList<>();
		for (Object object : objects) {
			list.add(object == null ? "" : object);
		}
		return list;
	}

	protected void createHeaderCell(Workbook workbook, Sheet sheet, Row header, int column, String title) {
		sheet.setColumnWidth(column, 10000);
		Cell headerCell = header.createCell(column);
		headerCell.setCellValue(title);
		headerCell.setCellStyle(getHeaderStyle(workbook));
	}

	protected CellStyle getHeaderStyle(Workbook workbook) {
		final CellStyle headerStyle = workbook.createCellStyle();
		headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		final XSSFFont font = ((XSSFWorkbook) workbook).createFont();
		font.setFontName("Arial");
		font.setFontHeightInPoints((short) 16);
		font.setBold(true);
		headerStyle.setFont(font);

		return headerStyle;
	}

	protected void writeValues(Workbook workbook, Sheet sheet, int rowNumber, Object... values) {
		CellStyle style = workbook.createCellStyle();
		style.setWrapText(true);

		Row row = sheet.createRow(rowNumber);
		for (int i = 0; i < values.length; i++) {
			Cell cell = row.createCell(i);
			Object value = values[i];
			if (value instanceof Number) {
				cell.setCellValue(((Number) value).doubleValue());
			} else if (value == null) {
				cell.setBlank();
			} else {
				cell.setCellValue(String.valueOf(value));
			}

			cell.setCellStyle(style);
		}
	}
}
