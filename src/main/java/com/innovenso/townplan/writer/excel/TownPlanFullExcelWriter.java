package com.innovenso.townplan.writer.excel;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.ModelComponent;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.aspects.*;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.deployment.aws.*;
import com.innovenso.townplan.api.value.enums.Criticality;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionContext;
import com.innovenso.townplan.api.value.it.decision.DecisionOption;
import com.innovenso.townplan.api.value.it.principle.Principle;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.api.value.view.FlowView;
import com.innovenso.townplan.api.value.view.Step;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
public class TownPlanFullExcelWriter extends AbstractTownPlanExcelWriter {

	public TownPlanFullExcelWriter(@NonNull File targetBaseDirectory) {
		super(new File(targetBaseDirectory, "townplan"));
	}

	@Override
	protected void write(@NonNull TownPlan townPlan, @NonNull XSSFWorkbook workbook) {
		writeTimeMachine(workbook, townPlan);
		writeEnterprises(workbook, townPlan);
		writePrinciples(workbook, townPlan);
		writeProjects(workbook, townPlan);
		writeProjectMilestones(workbook, townPlan);
		writeBusinessCapabilities(workbook, townPlan);
		writeArchitectureBuildingBlocks(workbook, townPlan);
		writePlatforms(workbook, townPlan);
		writeSystems(workbook, townPlan);
		writeSystemIntegrations(workbook, townPlan);
		writeContainers(workbook, townPlan);
		writeTechnologies(workbook, townPlan);
		writeContainerTechnologyMapping(workbook, townPlan);
		writeEntities(workbook, townPlan);
		writeEntityFields(workbook, townPlan);
		writeBusinessActors(workbook, townPlan);
		writeAwsAccounts(workbook, townPlan);
		writeAwsRegions(workbook, townPlan);
		writeAwsAvailabilityZones(workbook, townPlan);
		writeAwsVpcs(workbook, townPlan);
		writeAwsNetworkAcls(workbook, townPlan);
		writeAwsRouteTables(workbook, townPlan);
		writeAwsSubnets(workbook, townPlan);
		writeAwsSecurityGroups(workbook, townPlan);
		writeAwsInstances(workbook, townPlan);
		writeAwsSecurityGroupInstanceMappings(workbook, townPlan);
		writeAwsSubnetInstanceMappings(workbook, townPlan);
		writeAwsAutoScalingGroups(workbook, townPlan);
		writeAwsAutoScalingGroupAvailabilityZones(workbook, townPlan);
		writeAwsAutoScalingGroupSubnets(workbook, townPlan);
		writeAwsAutoScalingGroupInstances(workbook, townPlan);
		writeAwsElasticIpAddresses(workbook, townPlan);
		writeSwots(workbook, townPlan);
		writeLifecycles(workbook, townPlan);
		writeArchitectureVerdicts(workbook, townPlan);
		writeDocumentations(workbook, townPlan);
		writeExternalIds(workbook, townPlan);
		writeSecurityConcerns(workbook, townPlan);
		writeSecurityImpacts(workbook, townPlan);
		writeFunctionalRequirements(workbook, townPlan);
		writeQualityAttributeRequirements(workbook, townPlan);
		writeConstraints(workbook, townPlan);
		writeFunctionalRequirementScores(workbook, townPlan);
		writeQualityAttributeScores(workbook, townPlan);
		writeConstraintScores(workbook, townPlan);
		writeCosts(workbook, townPlan);
		writeFatherTime(workbook, townPlan);
		writeAttachments(workbook, townPlan);
		writeRelationships(workbook, townPlan);
		writeRelationshipTechnologyMapping(workbook, townPlan);
		writeSystemIntegrationSteps(workbook, townPlan);
		writeDecisions(workbook, townPlan);
		writeDecisionContexts(workbook, townPlan);
		writeDecisionOptions(workbook, townPlan);
		writeFlowViews(workbook, townPlan);
	}

	private void writeEnterprises(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("Enterprises", workbook, "key", "title", "description");
		int row = 1;
		List<Enterprise> enterprises = townPlan.getElements(Enterprise.class).stream()
				.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey())).collect(Collectors.toList());
		for (int i = 0; i < enterprises.size(); i++) {
			Enterprise enterprise = enterprises.get(i);
			writeValues(workbook, sheet, i + 1, enterprise.getKey(), enterprise.getTitle(),
					enterprise.getDescription());
		}
	}

	private void writeProjects(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("IT Projects", workbook, "key", "title", "description", "type", "enterprise");
		int row = 1;
		List<ItProject> projects = townPlan.getElements(ItProject.class).stream()
				.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey())).collect(Collectors.toList());
		for (int i = 0; i < projects.size(); i++) {
			ItProject project = projects.get(i);
			writeValues(workbook, sheet, i + 1, project.getKey(), project.getTitle(), project.getDescription(),
					project.getType(), project.getEnterprise() == null ? "" : project.getEnterprise().getKey());
		}
	}

	private void writeProjectMilestones(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("IT Project Milestones", workbook, "key", "title", "description", "project",
				"sort key");
		int row = 1;
		List<ItProjectMilestone> milestones = townPlan.getElements(ItProjectMilestone.class).stream()
				.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey())).collect(Collectors.toList());
		for (int i = 0; i < milestones.size(); i++) {
			ItProjectMilestone milestone = milestones.get(i);
			writeValues(workbook, sheet, i + 1, milestone.getKey(), milestone.getTitle(), milestone.getDescription(),
					milestone.getProject() == null ? "" : milestone.getProject().getKey(), milestone.getSortKey());
		}
	}

	private void writeBusinessCapabilities(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("Business Capabilities - Domains", workbook, "key", "title", "description",
				"enterprise", "parent capability", "sort key");
		int row = 1;
		List<BusinessCapability> enterprises = townPlan.getElements(BusinessCapability.class).stream()
				.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey())).collect(Collectors.toList());
		for (int i = 0; i < enterprises.size(); i++) {
			BusinessCapability capability = enterprises.get(i);
			String parent = capability.getParent().isPresent() ? capability.getParent().get().getKey() : "";
			writeValues(workbook, sheet, i + 1, capability.getKey(), capability.getTitle(), capability.getDescription(),
					capability.getEnterprise().getKey(), parent, capability.getSortKey());
		}
	}

	private void writeArchitectureBuildingBlocks(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("Architecture Building Blocks", workbook, "key", "title", "description",
				"enterprise");
		int row = 1;
		List<ArchitectureBuildingBlock> buildingBlocks = townPlan.getElements(ArchitectureBuildingBlock.class).stream()
				.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey())).collect(Collectors.toList());
		for (int i = 0; i < buildingBlocks.size(); i++) {
			ArchitectureBuildingBlock buildingBlock = buildingBlocks.get(i);
			writeValues(workbook, sheet, i + 1, buildingBlock.getKey(), buildingBlock.getTitle(),
					buildingBlock.getDescription(),
					buildingBlock.getEnterprise() == null ? "" : buildingBlock.getEnterprise().getKey());
		}
	}

	private void writePlatforms(Workbook workbook, TownPlan townPlan) {
		writeElements(townPlan, "IT Platforms", workbook, List.of("key", "title", "description"), ItPlatform.class,
				platform -> listOf(platform.getKey(), platform.getTitle(), platform.getDescription()));
	}

	private void writeSystems(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("IT Systems", workbook, "key", "title", "description", "platform");
		int row = 1;
		List<ItSystem> itSystems = townPlan.getElements(ItSystem.class).stream()
				.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey())).collect(Collectors.toList());
		for (int i = 0; i < itSystems.size(); i++) {
			ItSystem itSystem = itSystems.get(i);
			writeValues(workbook, sheet, i + 1, itSystem.getKey(), itSystem.getTitle(), itSystem.getDescription(),
					itSystem.getPlatform().map(ItPlatform::getKey).orElse(""));
		}
	}

	private void writeSystemIntegrations(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("IT System Integrations", workbook, "key", "title", "description", "source system",
				"target system", "volume", "frequency", "criticality", "criticality description", "resilience");
		int row = 1;
		List<ItSystemIntegration> integrations = townPlan.getElements(ItSystemIntegration.class).stream()
				.sorted(Comparator.comparing(Element::getKey)).collect(Collectors.toList());
		for (int i = 0; i < integrations.size(); i++) {
			ItSystemIntegration integration = integrations.get(i);
			writeValues(workbook, sheet, i + 1, integration.getKey(), integration.getTitle(),
					integration.getDescription(), integration.getSourceSystem().getKey(),
					integration.getTargetSystem().getKey(), integration.getVolume().orElse(""),
					integration.getFrequency().orElse(""),
					integration.getCriticality().orElse(Criticality.UNKNOWN).getName(),
					integration.getCriticalityDescription().orElse(""), integration.getResilience().orElse(""));
		}
	}

	private void writeContainers(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("IT Containers", workbook, "key", "title", "description", "type", "system");
		int row = 1;
		List<ItContainer> itContainers = townPlan.getElements(ItContainer.class).stream()
				.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey())).collect(Collectors.toList());
		for (int i = 0; i < itContainers.size(); i++) {
			ItContainer itContainer = itContainers.get(i);
			String system = itContainer.getSystem() != null ? itContainer.getSystem().getKey() : "";
			writeValues(workbook, sheet, i + 1, itContainer.getKey(), itContainer.getTitle(),
					itContainer.getDescription(), itContainer.getContainerType().getValue(), system);
		}
	}

	private void writeTechnologies(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("IT Technologies", workbook, "key", "title", "description", "type", "recommendation",
				"technology type");
		int row = 1;
		List<Technology> technologies = townPlan.getElements(Technology.class).stream()
				.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey())).collect(Collectors.toList());
		for (int i = 0; i < technologies.size(); i++) {
			Technology technology = technologies.get(i);
			writeValues(workbook, sheet, i + 1, technology.getKey(), technology.getTitle(), technology.getDescription(),
					technology.getType(),
					technology.getRecommendation() == null ? "" : technology.getRecommendation().getName(),
					technology.getTechnologyType() == null ? "" : technology.getTechnologyType().getName());
		}
	}

	private void writeContainerTechnologyMapping(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("IT Container Technology Mapping", workbook, "container", "technology");
		int row = 1;
		List<ItContainer> itContainers = townPlan.getElements(ItContainer.class).stream()
				.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey())).collect(Collectors.toList());
		for (ItContainer itContainer : itContainers) {
			Set<Technology> technologies = itContainer.getTechnologies();
			for (Technology technology : technologies) {
				writeValues(workbook, sheet, row++, itContainer.getKey(), technology.getKey());
			}
		}
	}

	private void writeRelationshipTechnologyMapping(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("Relationship Technology Mapping", workbook, "relationship", "technology");
		int row = 1;
		List<Relationship> relationships = townPlan.getAllRelationships().stream()
				.sorted(Comparator.comparing(Concept::getKey)).collect(Collectors.toList());
		for (Relationship relationship : relationships) {
			Set<Technology> technologies = relationship.getTechnologies();
			for (Technology technology : technologies) {
				writeValues(workbook, sheet, row++, relationship.getKey(), technology.getKey());
			}
		}
	}

	private void writeEntities(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("Data Entities", workbook, "key", "title", "description", "type", "sensitivity",
				"commercial value", "consent needed", "master system", "business capability", "enterprise");
		int row = 1;
		List<DataEntity> dataEntities = townPlan.getElements(DataEntity.class).stream()
				.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey())).collect(Collectors.toList());
		for (int i = 0; i < dataEntities.size(); i++) {
			DataEntity dataEntity = dataEntities.get(i);
			writeValues(workbook, sheet, i + 1, dataEntity.getKey(), dataEntity.getTitle(), dataEntity.getDescription(),
					dataEntity.getEntityType().getValue(), dataEntity.getSensitivity(), dataEntity.getCommercialValue(),
					String.valueOf(dataEntity.isConsentNeeded()),
					dataEntity.getMasterSystem() == null ? "" : dataEntity.getMasterSystem().getKey(),
					dataEntity.getBusinessCapability() == null ? "" : dataEntity.getBusinessCapability().getKey(),
					dataEntity.getEnterprise() == null ? "" : dataEntity.getEnterprise().getKey());
		}
	}

	private void writeEntityFields(Workbook workbook, TownPlan townPlan) {
		writeElements(
				townPlan, "Data Entity Fields", workbook, List.of("entity", "key", "title", "description", "mandatory",
						"unique", "constraints", "default value", "localization", "sort key", "type"),
				DataEntityField.class,
				field -> listOf(field.getDataEntity().getKey(), field.getKey(), field.getTitle(),
						field.getDescription(), String.valueOf(field.isMandatory()), String.valueOf(field.isUnique()),
						field.getFieldConstraints(), field.getDefaultValue(), field.getLocalization(),
						field.getSortKey(), field.getType()));
	}

	private void writeBusinessActors(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("Business Actors", workbook, "key", "title", "description", "type", "enterprise");
		int row = 1;
		List<BusinessActor> actors = townPlan.getElements(BusinessActor.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey)).collect(Collectors.toList());
		for (int i = 0; i < actors.size(); i++) {
			BusinessActor actor = actors.get(i);
			writeValues(workbook, sheet, i + 1, actor.getKey(), actor.getTitle(), actor.getDescription(),
					actor.getActorType().getValue(),
					actor.getEnterprise() == null ? "" : actor.getEnterprise().getKey());
		}
	}

	private void writeAwsRegions(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS Regions", workbook, "key", "title", "description");
		int row = 1;
		List<AwsRegion> regions = townPlan.getElements(AwsRegion.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey)).collect(Collectors.toList());
		for (int i = 0; i < regions.size(); i++) {
			AwsRegion region = regions.get(i);
			writeValues(workbook, sheet, i + 1, region.getKey(), region.getTitle(), region.getDescription());
		}
	}

	private void writeAwsVpcs(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS VPCs", workbook, "key", "title", "description", "region", "network mask",
				"account");
		int row = 1;
		List<AwsVpc> vpcs = townPlan.getElements(AwsVpc.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey)).collect(Collectors.toList());
		for (int i = 0; i < vpcs.size(); i++) {
			AwsVpc vpc = vpcs.get(i);
			writeValues(workbook, sheet, i + 1, vpc.getKey(), vpc.getTitle(), vpc.getDescription(),
					vpc.getRegion().getKey(), vpc.getNetworkMask(), vpc.getAccount().getKey());
		}
	}

	private void writeAwsAvailabilityZones(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS AZs", workbook, "key", "title", "description", "region");
		int row = 1;
		List<AwsAvailabilityZone> azs = townPlan.getElements(AwsAvailabilityZone.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey)).collect(Collectors.toList());
		for (int i = 0; i < azs.size(); i++) {
			AwsAvailabilityZone az = azs.get(i);
			writeValues(workbook, sheet, i + 1, az.getKey(), az.getTitle(), az.getDescription(),
					az.getRegion().getKey());
		}
	}

	private void writeAwsAutoScalingGroups(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS Autoscaling Groups", workbook, "key", "title", "description", "VPC");
		int row = 1;
		List<AwsAutoScalingGroup> autoScalingGroups = townPlan.getElements(AwsAutoScalingGroup.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey)).collect(Collectors.toList());
		for (int i = 0; i < autoScalingGroups.size(); i++) {
			AwsAutoScalingGroup autoScalingGroup = autoScalingGroups.get(i);
			writeValues(workbook, sheet, i + 1, autoScalingGroup.getKey(), autoScalingGroup.getTitle(),
					autoScalingGroup.getDescription(), autoScalingGroup.getVpc().getKey());
		}
	}

	private void writeAwsAutoScalingGroupAvailabilityZones(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS Autoscaling AZs", workbook, "autoscaling group", "availability zone");
		int row = 1;
		List<Pair<String, String>> mappings = townPlan.getElements(AwsAutoScalingGroup.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey))
				.flatMap(asg -> asg.getAvailabilityZones().stream().map(az -> Pair.of(asg.getKey(), az.getKey())))
				.collect(Collectors.toList());
		for (int i = 0; i < mappings.size(); i++) {
			Pair<String, String> mapping = mappings.get(i);
			writeValues(workbook, sheet, i + 1, mapping.getLeft(), mapping.getRight());
		}
	}

	private void writeAwsAutoScalingGroupSubnets(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS Autoscaling Subnets", workbook, "autoscaling group", "subnet");
		int row = 1;
		List<Pair<String, String>> mappings = townPlan.getElements(AwsAutoScalingGroup.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey))
				.flatMap(asg -> asg.getSubnets().stream().map(subnet -> Pair.of(asg.getKey(), subnet.getKey())))
				.collect(Collectors.toList());
		for (int i = 0; i < mappings.size(); i++) {
			Pair<String, String> mapping = mappings.get(i);
			writeValues(workbook, sheet, i + 1, mapping.getLeft(), mapping.getRight());
		}
	}

	private void writeAwsAutoScalingGroupInstances(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS Autoscaling Instances", workbook, "autoscaling group", "instance");
		int row = 1;
		List<Pair<String, String>> mappings = townPlan.getElements(AwsAutoScalingGroup.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey))
				.flatMap(asg -> asg.getInstances().stream().map(instance -> Pair.of(asg.getKey(), instance.getKey())))
				.collect(Collectors.toList());
		for (int i = 0; i < mappings.size(); i++) {
			Pair<String, String> mapping = mappings.get(i);
			writeValues(workbook, sheet, i + 1, mapping.getLeft(), mapping.getRight());
		}
	}

	private void writeAwsElasticIpAddresses(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS IP Addresses", workbook, "key", "title", "description", "region",
				"public IPv4 pool", "public IP address", "private IP address", "network interface owner",
				"network interface id", "network border group", "instance id", "domain", "customer owned IPv4 pool",
				"customer owned IP", "carrier IP", "association id", "allocation id", "account", "instance");

		int row = 1;
		List<AwsElasticIpAddress> ips = townPlan.getElements(AwsElasticIpAddress.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey)).collect(Collectors.toList());
		for (int i = 0; i < ips.size(); i++) {
			AwsElasticIpAddress ip = ips.get(i);
			writeValues(workbook, sheet, i + 1, ip.getKey(), ip.getTitle(), val(ip.getDescription()), ip.getRegion(),
					val(ip.getPublicIpv4Pool()), val(ip.getPublicIpAddress()), val(ip.getPrivateIpAddress()),
					val(ip.getNetworkInterfaceOwnerId()), val(ip.getNetworkInterfaceId()),
					val(ip.getNetworkBorderGroup()), val(ip.getInstanceId()), val(ip.getDomain()),
					val(ip.getCustomerOwnedIpv4Pool()), val(ip.getCustomerOwnedIp()), val(ip.getCarrierIp()),
					val(ip.getAssociationId()), val(ip.getAllocationId()), ip.getAccount().getKey(),
					ip.getInstance().map(ModelComponent::getKey).orElse(""));
		}
	}

	private String val(String input) {
		return input == null ? "" : input;
	}

	private void writeAwsNetworkAcls(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS Network ACL", workbook, "key", "title", "description", "VPC");
		int row = 1;
		List<AwsNetworkAcl> acls = townPlan.getElements(AwsNetworkAcl.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey)).collect(Collectors.toList());
		for (int i = 0; i < acls.size(); i++) {
			AwsNetworkAcl acl = acls.get(i);
			writeValues(workbook, sheet, i + 1, acl.getKey(), acl.getTitle(), acl.getDescription(),
					acl.getVpc().getKey());
		}
	}

	private void writeAwsRouteTables(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS Route Tables", workbook, "key", "title", "description", "VPC");
		int row = 1;
		List<AwsRouteTable> routeTables = townPlan.getElements(AwsRouteTable.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey)).collect(Collectors.toList());
		for (int i = 0; i < routeTables.size(); i++) {
			AwsRouteTable routeTable = routeTables.get(i);
			writeValues(workbook, sheet, i + 1, routeTable.getKey(), routeTable.getTitle(), routeTable.getDescription(),
					routeTable.getVpc().getKey());
		}
	}

	private void writeAwsSubnets(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS Subnets", workbook, "key", "title", "description", "VPC", "Availability zone",
				"Subnet mask", "Public");
		int row = 1;
		List<AwsSubnet> subnets = townPlan.getElements(AwsSubnet.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey)).collect(Collectors.toList());
		for (int i = 0; i < subnets.size(); i++) {
			AwsSubnet subnet = subnets.get(i);
			writeValues(workbook, sheet, i + 1, subnet.getKey(), subnet.getTitle(), subnet.getDescription(),
					subnet.getVpc().getKey(), subnet.getAvailabilityZone().getKey(), subnet.getSubnetMask(),
					String.valueOf(subnet.isPublicSubnet()));
		}
	}

	private void writeAwsSecurityGroups(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS Security Groups", workbook, "key", "title", "description", "VPC");
		int row = 1;
		List<AwsSecurityGroup> securityGroups = townPlan.getElements(AwsSecurityGroup.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey)).collect(Collectors.toList());
		for (int i = 0; i < securityGroups.size(); i++) {
			AwsSecurityGroup securityGroup = securityGroups.get(i);
			writeValues(workbook, sheet, i + 1, securityGroup.getKey(), securityGroup.getTitle(),
					securityGroup.getDescription(), securityGroup.getVpc().getKey());
		}
	}

	private void writeAwsAccounts(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS Accounts", workbook, "key", "title", "description", "account ID");
		int row = 1;
		List<AwsAccount> accounts = townPlan.getElements(AwsAccount.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey)).collect(Collectors.toList());
		for (int i = 0; i < accounts.size(); i++) {
			AwsAccount account = accounts.get(i);
			writeValues(workbook, sheet, i + 1, account.getKey(), account.getTitle(), account.getDescription(),
					account.getAccountId());
		}
	}

	private void writeAwsSubnetInstanceMappings(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS Subnet Map", workbook, "subnet", "instance");
		int row = 1;
		List<Pair<AwsSubnet, AwsInstance>> mappings = new ArrayList<>();
		townPlan.getElements(AwsSubnet.class).stream().sorted(Comparator.comparing(ModelComponent::getKey))
				.forEach(subnet -> {
					subnet.getInstances().stream().sorted(Comparator.comparing(ModelComponent::getKey))
							.forEach(instance -> {
								mappings.add(Pair.of(subnet, instance));
							});
				});
		for (int i = 0; i < mappings.size(); i++) {
			Pair<AwsSubnet, AwsInstance> mapping = mappings.get(i);
			writeValues(workbook, sheet, i + 1, mapping.getLeft().getKey(), mapping.getRight().getKey());
		}
	}

	private void writeAwsSecurityGroupInstanceMappings(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS Security Group Map", workbook, "security group", "instance");
		int row = 1;
		List<Pair<AwsSecurityGroup, AwsInstance>> mappings = new ArrayList<>();
		townPlan.getElements(AwsSecurityGroup.class).stream().sorted(Comparator.comparing(ModelComponent::getKey))
				.forEach(sg -> {
					sg.getInstances().stream().sorted(Comparator.comparing(ModelComponent::getKey))
							.forEach(instance -> {
								mappings.add(Pair.of(sg, instance));
							});
				});
		for (int i = 0; i < mappings.size(); i++) {
			Pair<AwsSecurityGroup, AwsInstance> mapping = mappings.get(i);
			writeValues(workbook, sheet, i + 1, mapping.getLeft().getKey(), mapping.getRight().getKey());
		}
	}

	private void writeAwsInstances(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("AWS Instances", workbook, "key", "title", "description", "instance type", "VPC",
				"account", "region");
		int row = 1;
		List<AwsInstance> instances = townPlan.getElements(AwsInstance.class).stream()
				.sorted(Comparator.comparing(ModelComponent::getKey)).collect(Collectors.toList());
		for (int i = 0; i < instances.size(); i++) {
			AwsInstance instance = instances.get(i);
			writeValues(workbook, sheet, i + 1, instance.getKey(), instance.getTitle(), instance.getDescription(),
					instance.getInstanceType().name(), instance.getVpc().map(ModelComponent::getKey).orElse(""),
					instance.getAccount().getKey(), instance.getRegion().map(ModelComponent::getKey).orElse(""));
		}
	}

	private void writeSwots(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("Aspects - SWOT", workbook, "concept key", "type", "description");
		int row = 1;
		List<? extends Concept> concepts = townPlan.getAllConcepts().stream().sorted().collect(Collectors.toList());
		for (Concept concept : concepts) {
			for (SWOT swot : concept.getSwots()) {
				writeValues(workbook, sheet, row++, concept.getKey(), swot.getType().name(), swot.getDescription());
			}
		}
	}

	private void writeDocumentations(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("Aspects - Documentation", workbook, "concept key", "type", "description",
				"sort key");
		int row = 1;
		List<? extends Concept> concepts = townPlan.getAllConcepts().stream().sorted().collect(Collectors.toList());
		for (Concept concept : concepts) {
			for (Documentation documentation : concept.getDocumentations()) {
				writeValues(workbook, sheet, row++, concept.getKey(), documentation.getType().name(),
						documentation.getDescription(), documentation.getSortKey());
			}
		}
	}

	private void writeSecurityConcerns(Workbook workbook, TownPlan townPlan) {
		withSheet("Aspects - Security Concern", workbook,
				List.of("concept key", "type", "title", "description", "sort key"),
				(sheet, counter) -> townPlan.getAllConcepts().stream().sorted(Comparator.comparing(Concept::getKey))
						.forEach(concept -> concept.getSecurityConcerns()
								.forEach(concern -> writeValues(workbook, sheet, counter.getAndIncrement(),
										concept.getKey(), concern.getConcernType().name(), concern.getTitle(),
										concern.getDescription(), concern.getSortKey()))));
	}

	private void writeSecurityImpacts(Workbook workbook, TownPlan townPlan) {
		withSheet("Aspects - Security Impact", workbook, List.of("concept key", "level", "type", "description"),
				(sheet, counter) -> townPlan.getAllConcepts().stream().sorted(Comparator.comparing(Concept::getKey))
						.forEach(concept -> concept.getSecurityImpacts()
								.forEach(impact -> writeValues(workbook, sheet, counter.getAndIncrement(),
										concept.getKey(), impact.getImpactLevel().name(), impact.getImpactType().name(),
										impact.getDescription()))));
	}

	private void writePrinciples(Workbook workbook, TownPlan townPlan) {
		withSheet("IT Principles", workbook, List.of("key", "title", "description", "type", "enterprise", "sort key"),
				(sheet, counter) -> townPlan.getElements(Principle.class).stream()
						.sorted(Comparator.comparing(Concept::getKey))
						.forEach(principle -> writeValues(workbook, sheet, counter.getAndIncrement(),
								principle.getKey(), principle.getTitle(), principle.getDescription(),
								principle.getType(), principle.getEnterprise().getKey(), principle.getSortKey())));
	}

	private void writeDecisions(Workbook workbook, TownPlan townPlan) {
		withSheet("Architecture Decisions", workbook,
				List.of("key", "title", "description", "status", "outcome", "enterprise"),
				(sheet, counter) -> townPlan.getElements(Decision.class).stream()
						.sorted(Comparator.comparing(Concept::getKey))
						.forEach(decision -> writeValues(workbook, sheet, counter.getAndIncrement(), decision.getKey(),
								decision.getTitle(), decision.getDescription(), decision.getStatus().getValue(),
								decision.getOutcome(), decision.getEnterprise().getKey())));
	}

	private void writeDecisionContexts(Workbook workbook, TownPlan townPlan) {
		writeElements(townPlan, "Decision Contexts", workbook,
				List.of("key", "title", "description", "sort key", "decision", "type"), DecisionContext.class,
				decisionContext -> listOf(decisionContext.getKey(), decisionContext.getTitle(),
						decisionContext.getDescription(), decisionContext.getSortKey(),
						decisionContext.getDecision().getKey(), decisionContext.getContextType().getValue()));
	}

	private void writeDecisionOptions(Workbook workbook, TownPlan townPlan) {
		writeElements(townPlan, "Decision Options", workbook,
				List.of("key", "title", "description", "sort key", "decision", "verdict"), DecisionOption.class,
				decisionOption -> listOf(decisionOption.getKey(), decisionOption.getTitle(),
						decisionOption.getDescription(), decisionOption.getSortKey(),
						decisionOption.getDecision().getKey(), decisionOption.getVerdict().getValue()));
	}

	private void writeConstraints(Workbook workbook, TownPlan townPlan) {
		withSheet("Aspects - Constraints", workbook,
				List.of("concept key", "title", "description", "sort key", "weight", "key"),
				(sheet, counter) -> townPlan.getAllConcepts().stream().sorted(Comparator.comparing(Concept::getKey))
						.forEach(concept -> concept.getConstraints()
								.forEach(constraint -> writeValues(workbook, sheet, counter.getAndIncrement(),
										concept.getKey(), constraint.getTitle(), constraint.getDescription(),
										constraint.getSortKey(), constraint.getWeight().getValue(),
										constraint.getKey()))));
	}

	private void writeAttachments(Workbook workbook, TownPlan townPlan) {
		withSheet("Aspects - Attachments", workbook, List.of("concept key", "title", "description", "CDN content URL"),
				(sheet, counter) -> townPlan.getAllConcepts().stream().sorted(Comparator.comparing(Concept::getKey))
						.forEach(concept -> concept.getAttachments()
								.forEach(attachment -> writeValues(workbook, sheet, counter.getAndIncrement(),
										concept.getKey(), attachment.getTitle(), attachment.getDescription(),
										attachment.getCdnContentUrl()))));
	}

	private void writeTimeMachine(Workbook workbook, TownPlan townPlan) {
		withSheet("Time Machine", workbook, List.of("year", "month", "day", "title", "diagrams needed"),
				(sheet, counter) -> townPlan.getKeyPointsInTime()
						.forEach(keyPointInTime -> writeValues(workbook, sheet, counter.getAndIncrement(),
								keyPointInTime.getDate().getYear(), keyPointInTime.getDate().getMonthValue(),
								keyPointInTime.getDate().getDayOfMonth(), keyPointInTime.getTitle(),
								keyPointInTime.isDiagramsNeeded())));
	}

	private void writeFatherTime(Workbook workbook, TownPlan townPlan) {
		withSheet("Aspects - Father Time", workbook,
				List.of("concept key", "title", "description", "year", "month", "day", "type"),
				(sheet, counter) -> townPlan.getAllConcepts().stream().sorted(Comparator.comparing(Concept::getKey))
						.forEach(concept -> concept.getFatherTimes()
								.forEach(fatherTime -> writeValues(workbook, sheet, counter.getAndIncrement(),
										concept.getKey(), fatherTime.getTitle(), fatherTime.getDescription(),
										fatherTime.getPointInTime().getYear(),
										fatherTime.getPointInTime().getMonthValue(),
										fatherTime.getPointInTime().getDayOfMonth(),
										fatherTime.getFatherTimeType().getValue()))));
	}

	private void writeCosts(Workbook workbook, TownPlan townPlan) {
		withSheet("Aspects - Cost", workbook,
				List.of("concept key", "title", "description", "category", "fiscal year", "cost per unit",
						"unit of measure", "number of units", "cost type"),
				(sheet, counter) -> townPlan.getAllConcepts().stream().sorted(Comparator.comparing(Concept::getKey))
						.forEach(concept -> concept.getCosts()
								.forEach(cost -> writeValues(workbook, sheet, counter.getAndIncrement(),
										concept.getKey(), cost.getTitle(), cost.getDescription(), cost.getCategory(),
										cost.getFiscalYear(), cost.getCostPerUnit(), cost.getUnitOfMeasure(),
										cost.getNumberOfUnits(), cost.getCostType().getValue()))));
	}

	private void writeFunctionalRequirements(Workbook workbook, TownPlan townPlan) {
		withSheet("Aspects - Functional Reqs", workbook,
				List.of("concept key", "title", "description", "sort key", "weight", "key"),
				(sheet, counter) -> townPlan.getAllConcepts().stream().sorted(Comparator.comparing(Concept::getKey))
						.forEach(concept -> concept.getFunctionalRequirements()
								.forEach(functionalRequirement -> writeValues(workbook, sheet,
										counter.getAndIncrement(), concept.getKey(), functionalRequirement.getTitle(),
										functionalRequirement.getDescription(), functionalRequirement.getSortKey(),
										functionalRequirement.getWeight().getValue(),
										functionalRequirement.getKey()))));
	}

	private void writeFunctionalRequirementScores(Workbook workbook, TownPlan townPlan) {
		withSheet("Aspects - Functional Scores", workbook,
				List.of("concept key", "requirement", "description", "weight", "key"),
				(sheet, counter) -> townPlan.getAllConcepts().stream().sorted(Comparator.comparing(Concept::getKey))
						.forEach(concept -> concept.getFunctionalRequirementScores()
								.forEach(score -> writeValues(workbook, sheet, counter.getAndIncrement(),
										concept.getKey(), score.getFunctionalRequirement().getKey(),
										score.getDescription(), score.getWeight().getValue(), score.getKey()))));
	}

	private void writeQualityAttributeScores(Workbook workbook, TownPlan townPlan) {
		withSheet("Aspects - QAR Scores", workbook,
				List.of("concept key", "requirement", "description", "weight", "key"),
				(sheet, counter) -> townPlan.getAllConcepts().stream().sorted(Comparator.comparing(Concept::getKey))
						.forEach(concept -> concept.getQualityAttributeRequirementScores()
								.forEach(score -> writeValues(workbook, sheet, counter.getAndIncrement(),
										concept.getKey(), score.getQualityAttributeRequirement().getKey(),
										score.getDescription(), score.getWeight().getValue(), score.getKey()))));
	}

	private void writeConstraintScores(Workbook workbook, TownPlan townPlan) {
		withSheet("Aspects - Constraint Scores", workbook,
				List.of("concept key", "requirement", "description", "weight", "key"),
				(sheet, counter) -> townPlan.getAllConcepts().stream().sorted(Comparator.comparing(Concept::getKey))
						.forEach(concept -> concept.getConstraintScores()
								.forEach(score -> writeValues(workbook, sheet, counter.getAndIncrement(),
										concept.getKey(), score.getConstraint().getKey(), score.getDescription(),
										score.getWeight().getValue(), score.getKey()))));
	}

	private void writeExternalIds(Workbook workbook, TownPlan townPlan) {
		withSheet("Aspects - External ID", workbook, List.of("concept key", "type", "value"),
				(sheet, counter) -> townPlan.getAllConcepts().stream().sorted(Comparator.comparing(Concept::getKey))
						.forEach(concept -> concept.getExternalIds()
								.forEach(externalId -> writeValues(workbook, sheet, counter.getAndIncrement(),
										concept.getKey(), externalId.getExternalIdType().name(),
										externalId.getValue()))));
	}

	private void writeQualityAttributeRequirements(Workbook workbook, TownPlan townPlan) {
		withSheet("Aspects - QAR", workbook,
				List.of("concept key", "title", "source of stimulus", "stimulus", "environment", "response",
						"response measure", "sort key", "weight", "key"),
				(sheet, counter) -> townPlan.getAllConcepts().stream().sorted(Comparator.comparing(Concept::getKey))
						.forEach(concept -> concept.getQualityAttributes()
								.forEach(functionalRequirement -> writeValues(workbook, sheet,
										counter.getAndIncrement(), concept.getKey(), functionalRequirement.getTitle(),
										functionalRequirement.getSourceOfStimulus(),
										functionalRequirement.getStimulus(), functionalRequirement.getEnvironment(),
										functionalRequirement.getResponse(), functionalRequirement.getResponseMeasure(),
										functionalRequirement.getSortKey(),
										functionalRequirement.getWeight().getValue(),
										functionalRequirement.getKey()))));
	}

	private void writeContentDistributions(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("Aspects - Content Distribution", workbook, "model component key", "type", "url");
		int row = 1;
		List<? extends ModelComponent> modelComponents = townPlan.getAllModelComponents().stream()
				.sorted(Comparator.comparing(ModelComponent::getKey)).collect(Collectors.toList());
		for (ModelComponent modelComponent : modelComponents) {
			for (ContentDistribution contentDistribution : modelComponent.getContentDistributions()) {
				writeValues(workbook, sheet, row++, modelComponent.getKey(), contentDistribution.getOutputType().name(),
						contentDistribution.getUrl());
			}
		}
	}

	private void writeLifecycles(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("Aspects - Lifecycle", workbook, "concept key", "type", "description");
		int row = 1;
		List<? extends Concept> concepts = townPlan.getAllConcepts().stream().sorted().collect(Collectors.toList());
		for (Concept concept : concepts) {
			Lifecycle lifecycle = concept.getLifecycle();
			if (lifecycle != null && !lifecycle.getType().equals(LifecyleType.UNKNOWN))
				writeValues(workbook, sheet, row++, concept.getKey(), lifecycle.getType().name(),
						lifecycle.getDescription());
		}
	}

	private void writeArchitectureVerdicts(Workbook workbook, TownPlan townPlan) {
		withSheet("Aspects - architecture verdict", workbook, List.of("concept key", "verdict", "description"),
				(sheet, counter) -> townPlan.getAllConcepts().stream()
						.forEach(concept -> writeValues(workbook, sheet, counter.getAndIncrement(), concept.getKey(),
								concept.getArchitectureVerdict().getVerdictType().getValue(),
								concept.getArchitectureVerdict().getDescription())));
	}

	private void writeRelationships(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("Relationships", workbook, "source", "target", "title", "description",
				"bidirectional", "type", "influence", "interest", "integration identifier");
		int row = 1;
		List<Relationship> relationships = townPlan.getAllRelationships().stream()
				.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey())).collect(Collectors.toList());
		for (int i = 0; i < relationships.size(); i++) {
			Relationship relationship = relationships.get(i);
			writeValues(workbook, sheet, i + 1, relationship.getSource().getKey(), relationship.getTarget().getKey(),
					relationship.getTitle(), relationship.getDescription(),
					String.valueOf(relationship.isBidirectional()), relationship.getRelationshipType().name(),
					relationship.getInfluence(), relationship.getInterest(),
					relationship.getSystemIntegration().map(ModelComponent::getKey).orElse(""));
		}
	}

	private String getStringValue(Row row, int columnIndex) {
		Cell cell = row.getCell(columnIndex);
		if (cell == null)
			return "";
		return cell.getStringCellValue();
	}

	private String getKeyValue(Row row, int columnIndex) {
		return getStringValue(row, columnIndex).replaceAll(" ", "_").replaceAll("-", "_");
	}

	private void writeFlowViews(Workbook workbook, TownPlan townPlan) {
		Sheet sheet = createSheet("Flow Views", workbook, "number", "key", "title", "description", "show step counter");
		int row = 0;
		List<FlowView> flowViews = townPlan.getFlowViews().stream().sorted(Comparator.comparing(ModelComponent::getKey))
				.collect(Collectors.toList());
		for (FlowView flowView : flowViews) {
			row++;
			writeValues(workbook, sheet, row, row, flowView.getKey(), flowView.getTitle(), flowView.getDescription(),
					flowView.isShowStepCounter());
			writeFlowViewMappings(workbook, townPlan, row, flowView);
		}
	}

	private void writeFlowViewMappings(Workbook workbook, TownPlan townPlan, int viewNumber, FlowView flowView) {
		Sheet sheet = createSheet("Flow View - " + String.valueOf(viewNumber), workbook, "source", "target", "type",
				"title override", "is response");
		int row = 1;
		List<Step> flowViewSteps = flowView.getSteps();
		for (Step step : flowViewSteps) {
			writeValues(workbook, sheet, row++, step.getRelationship().getSource().getKey(),
					step.getRelationship().getTarget().getKey(), step.getRelationship().getRelationshipType().name(),
					step.getTitle().orElse(""), String.valueOf(step.isResponse()));
		}
	}

	private void writeSystemIntegrationSteps(Workbook workbook, TownPlan townPlan) {
		townPlan.getElements(ItSystemIntegration.class).stream().forEach(integration -> {
			Sheet sheet = createSheet(DigestUtils.sha1Hex(integration.getKey().getBytes()).substring(0, 31), workbook,
					"source", "target", "type", "title override", "is response");
			int row = 1;
			List<Step> integrationSteps = integration.getSteps();
			for (Step step : integrationSteps) {
				writeValues(workbook, sheet, row++, step.getRelationship().getSource().getKey(),
						step.getRelationship().getTarget().getKey(),
						step.getRelationship().getRelationshipType().name(), step.getTitle().orElse(""),
						String.valueOf(step.isResponse()));
			}
		});
	}
}
