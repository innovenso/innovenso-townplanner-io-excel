package com.innovenso.townplan.writer.excel;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.aspects.ExternalId;
import com.innovenso.townplan.api.value.aspects.ExternalIdType;
import com.innovenso.townplan.api.value.it.ItSystemIntegration;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.util.List;
import java.util.Optional;

public class TownPlanSystemIntegrationListExcelWriter extends AbstractTownPlanExcelWriter {
	public TownPlanSystemIntegrationListExcelWriter(@NonNull File targetBaseDirectory) {
		super(new File(targetBaseDirectory, "integrations"));
	}

	@Override
	protected void write(@NonNull TownPlan townPlan, @NonNull XSSFWorkbook workbook) {
		withSheet("Sparx Export", workbook,
				List.of("number", "title", "description", "sparx id", "source system sparx id",
						"target system sparx id", "lifecycle", "verdict"),
				(sheet, counter) -> townPlan.getElements(ItSystemIntegration.class)
						.forEach(concept -> writeValues(workbook, sheet, counter.getAndIncrement(),
								getIntegrationNumber(concept), getIntegrationName(concept), concept.getDescription(),
								getSparxExternalId(concept), getSparxExternalId(concept.getSourceSystem()),
								getSparxExternalId(concept.getTargetSystem()), getLifecycle(concept),
								getVerdict(concept))));
	}

	private String getIntegrationNumber(Concept concept) {
		String[] parts = StringUtils.split(concept.getTitle(), " ", 2);
		if (parts.length > 0)
			return parts[0];
		else
			return "";
	}

	private String getIntegrationName(Concept concept) {
		String[] parts = StringUtils.split(concept.getTitle(), " ", 2);
		if (parts.length > 1)
			return parts[1];
		else
			return "";
	}

	private String getSparxExternalId(Concept concept) {
		return concept.getExternalIds().stream()
				.filter(externalId -> externalId.getExternalIdType().equals(ExternalIdType.SPARX)).findFirst()
				.map(ExternalId::getValue).orElse("");
	}

	private String getLifecycle(Concept concept) {
		return Optional.ofNullable(concept.getLifecycle()).map(lifecycle -> lifecycle.getType().getValue()).orElse("");
	}

	private String getVerdict(Concept concept) {
		return Optional.ofNullable(concept.getArchitectureVerdict()).map(verdict -> verdict.getVerdictType().getValue())
				.orElse("");
	}

	@Override
	protected String getExcelOutputFileName(@NonNull TownPlan townPlan) {
		return super.getExcelOutputFileName(townPlan) + "-integrations";
	}
}
