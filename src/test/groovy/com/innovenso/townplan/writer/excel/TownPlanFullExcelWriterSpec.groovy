package com.innovenso.townplan.writer.excel

import com.google.common.io.Files
import com.innovenso.eventsourcing.api.id.EntityId
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.reader.exceptions.TownPlanReaderException
import com.innovenso.townplan.repository.FileSystemAssetRepository
import org.apache.poi.openxml4j.util.ZipSecureFile
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import spock.lang.Specification

import java.nio.file.Path
import java.time.LocalDate

class TownPlanFullExcelWriterSpec extends Specification {
	def writePath = Files.createTempDir().getAbsolutePath()
	def assetRepository = new FileSystemAssetRepository(writePath, "https://test.com")
	def writer = new TownPlanExcelWriter(assetRepository, writePath)
	def townPlan = new TownPlanImpl(new EntityId("test"))
	def samples = new SampleFactory(townPlan)

	def "enterprises are written to excel"() {
		given:
		samples.enterprise()
		when:
		writer.write(townPlan)
		then:
		assetRepository.objectNames
		hasRows("Enterprises", 1)
	}

	def "capabilities are written to excel"() {
		given:
		def enterprise = samples.enterprise()
		4.times {
			samples.capability(enterprise)
		}
		when:
		writer.write(townPlan)
		then:
		assetRepository.objectNames
		hasRows("Business Capabilities - Domains", 4)
	}

	def "time machine is written to excel"() {
		given:
		def enterprise = samples.enterprise()
		samples.keyPointInTime(LocalDate.now().plusDays(100))
		when:
		writer.write(townPlan)
		then:
		assetRepository.objectNames
		hasRows("Time Machine", 2)
	}

	def "principles are written to excel"() {
		given:
		int number = samples.randomInt(100)
		def enterprise = samples.enterprise()
		number.times {
			samples.principle(enterprise)
		}
		when:
		writer.write(townPlan)
		then:
		assetRepository.objectNames
		hasRows("IT Principles", number)
	}

	def "projects are written to excel"() {
		given:
		int number = samples.randomInt(100)
		def enterprise = samples.enterprise()
		number.times {
			samples.project(enterprise)
		}
		when:
		writer.write(townPlan)
		then:
		assetRepository.objectNames
		hasRows("IT Projects", number)
	}

	def "project milestones are written to excel"() {
		given:
		int number = samples.randomInt(100)
		def enterprise = samples.enterprise()
		number.times {
			def project = samples.project(enterprise)
			2.times {
				samples.milestone(project)
			}
		}
		when:
		writer.write(townPlan)
		then:
		assetRepository.objectNames
		hasRows("IT Projects", number)
		hasRows("IT Project Milestones", number * 2)
	}

	def "building blocks are written to excel"() {
		given:
		int number = samples.randomInt(100)
		def enterprise = samples.enterprise()
		number.times {
			samples.buildingBlock(enterprise)
		}
		when:
		writer.write(townPlan)
		then:
		assetRepository.objectNames
		hasRows("Architecture Building Blocks", number)
	}

	def "platforms are written to excel"() {
		given:
		int number = samples.randomInt(100)
		def enterprise = samples.enterprise()
		number.times {
			samples.platform(samples.id(), samples.title())
		}
		when:
		writer.write(townPlan)
		then:
		assetRepository.objectNames
		hasRows("IT Platforms", number)
	}


	boolean hasRows(String sheetName, int number) {
		File excelFile = Path.of(writePath, "excel/townplan/test.xlsx").toFile()
		try {
			InputStream inputStream = new FileInputStream(excelFile)
			ZipSecureFile.setMinInflateRatio(0.001)
			XSSFWorkbook workbook = new XSSFWorkbook(inputStream)
			XSSFSheet sheet = workbook.getSheet(sheetName)
			int lastRowNumber = sheet.getLastRowNum()
			println "last row number in sheet ${sheetName} is ${lastRowNumber}"
			return lastRowNumber == number
		} catch (IOException e) {
			throw new TownPlanReaderException(e);
		}
	}
}
